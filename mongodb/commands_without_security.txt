Migration Command
=========================
mongoimport --host localhost:27017 --db=Assignment --collection=interest --file=interestDB_user.json
To get the count based on the entities:
=========================
db.getCollection('interest').aggregate([{$group:{_id:"$entityType", count:{"$sum":1}}}])
To get the indexes in DB 
=========================
db.getCollection('interest').getIndexes()
To get index in mongodb
=========================
db.getCollection('interest').createIndex({"location":"2dsphere", "entityType":1})

Query to get the points near lat:12.924033, long:77.513392
============================================================
db.getCollection('interest').aggregate([{$geoNear: {near: { type: "Point", coordinates: [ 77.513392, 12.924033 ] },distanceField: "dist.calculated",maxDistance: 900,spherical: true}}, {$group:{_id:"$entityType",count:{"$sum":1}}}])
db.getCollection('interest').aggregate([{
     $geoNear: {
        near: { type: "Point", coordinates: [ 77.513392 , 12.924033 ] },
        distanceField: "calculatedDistance",
        maxDistance: 900,
        spherical: true
     }
},{$project:{entityType:1, calculatedDistance:1}}])

