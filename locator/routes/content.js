var MongoClient = require('mongodb').MongoClient;

function contentHandler(req, res){
    MongoClient.connect("mongodb://localhost:27017/Assignment", function(err, db){
        if(err){
            res.send(500);
        } else {
            console.log("longitude:", req.body.longitude , " latitude:",  req.body.latitude, typeof( Number(req.body.latitude)), typeof( Number(req.body.longitude)));
            collection = db.collection('interest');
            collection.aggregate([
                {
                    $geoNear: {
                        near: { type: "Point", coordinates: [ Number(req.body.longitude) , Number(req.body.latitude) ] },
                        distanceField: "calculatedDistance",
                        maxDistance: 900,
                        spherical: true
                    }
                },
                {$project:
                    {entityType: 1, latitude:1, longitude:1, calculatedDistance: 1, _id: 0}
                }], function(err, data){
                    console.log("Fetched Data:",data, err);
                    res.render('welcomePage', {title:"Locator App", navItems:data});
                    db.close();
            })
        }
    })
}

exports.contentHandler = contentHandler;