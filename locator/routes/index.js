var express = require('express');
var content = require('./content');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  test = [];
  test.push(1);
  test.push(2);
  test.push(3);
  res.render('welcomePage', { title: 'Locator App', navItems: ['a', 'b', 'c'] });
});
router.post('/', function(req, res, next){
  console.log(req.body);
  content.contentHandler(req,res);
  //res.send(200);
})
router.get('/maps', function(req, res, next) {
  test = [];
  test.push(1);
  test.push(2);
  test.push(3);
  res.render('maps', { title: 'Locator App', navItems: ['a', 'b', 'c'] });
});

module.exports = router;
