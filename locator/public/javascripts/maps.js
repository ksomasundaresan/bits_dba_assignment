// *
// * Add multiple markers
// * 2013 - en.marnoto.com
// *

// necessary variables
var map;
var infoWindow;

// markersData variable stores the information necessary to each marker
var markersData = [
   {
      lat: 12.901685,
      lng: 77.6105,
      name: "Camping Praia da Barra",
      address1:"Rua Diogo Cão, 125",
      address2: "Praia da Barra",
      postalCode: "3830-772 Gafanha da Nazaré" // don't insert comma in the last item of each marker
   },
   {
      lat: 12.902685,
      lng: 77.6105,
      name: "Camping Costa Nova",
      address1:"Quinta dos Patos, n.º 2",
      address2: "Praia da Costa Nova",
      postalCode: "3830-453 Gafanha da Encarnação" // don't insert comma in the last item of each marker
   },
   {
      lat: 12.903685,
      lng: 77.6105,
      name: "Camping Gafanha da Nazaré",
      address1:"Rua dos Balneários do Complexo Desportivo",
      address2: "Gafanha da Nazaré",
      postalCode: "3830-225 Gafanha da Nazaré" // don't insert comma in the last item of each marker
   } // don't insert comma in the last item
];

function testFunction() {
      console.log("TEst pass:", document.getElementById('map-canvas').getAttribute("attr1"));
      markersData = document.getElementById('map-canvas').getAttribute("attr1");
      displayMarkers();
}

function initialize() {
   console.log("INIT 1");
   var mapOptions = { }
   if (markersData === 0) {
      mapOptions = {
            center: new google.maps.LatLng(12.954517,77.3507356),
            zoom: 0,
            mapTypeId: 'roadmap',
      };
   } else {
      mapOptions = {
            center: new google.maps.LatLng(markersData[0].latitude,markersData[0].longitude),
            zoom: 0,
            mapTypeId: 'roadmap',
      } 
   }
   console.log("INIT 2");
   if (map)
      delete map;
   console.log("INIT 3");
   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
   console.log("INIT 4");
   // a new Info Window is created
   infoWindow = new google.maps.InfoWindow();
   console.log("INIT 5");
   // Event that closes the Info Window with a click on the map
   google.maps.event.addListener(map, 'click', function() {
      infoWindow.close();
   });
   console.log("INIT 6");
   // Finally displayMarkers() function is called to begin the markers creation
   displayMarkers();
}
google.maps.event.addDomListener(window, 'load', initialize);


// This function will iterate over markersData array
// creating markers with createMarker function
function displayMarkers(){
      console.log("Display 1");
   // this variable sets the map bounds according to markers position
   var bounds = new google.maps.LatLngBounds();
   console.log("Display 2");
   // for loop traverses markersData array calling createMarker function for each marker 
   for (var i = 0; i < markersData.length; i++){
      console.log("Display 3, ", i);
      var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
      var name = markersData[i].name;
      var address1 = markersData[i].address1;
      var address2 = markersData[i].address2;
      var postalCode = markersData[i].postalCode;

      createMarker(latlng, name, address1, address2, postalCode);

      // marker position is added to bounds variable
      bounds.extend(latlng);  
   }
   console.log("Display 4");

   // Finally the bounds variable is used to set the map bounds
   // with fitBounds() function
   map.fitBounds(bounds);
   console.log("Display 5");
}

// This function creates each marker and it sets their Info Window content
function createMarker(latlng, name, address1, address2, postalCode){
      console.log("createMarker 1");
   var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      title: name
   });

   // This event expects a click on a marker
   // When this event is fired the Info Window content is created
   // and the Info Window is opened.
   console.log("createMarker 2");
   google.maps.event.addListener(marker, 'click', function() {
      
      // Creating the content to be inserted in the infowindow
      var iwContent = '<div id="iw_container">' +
            '<div class="iw_title">' + name + '</div>' +
         '<div class="iw_content">' + address1 + '<br />' +
         address2 + '<br />' +
         postalCode + '</div></div>';
      
      // including content to the Info Window.
      infoWindow.setContent(iwContent);

      // opening the Info Window in the current map and at the current marker location.
      infoWindow.open(map, marker);
   });
   console.log("createMarker 3");
}